@extends('index')

@section('content')
        <div class="job-content">
            <div class="job-title m-b-md">
                <p>BeoTuk 011</p>
            </div>

            <p><strong>Job advertisement</strong></p>

            <p>BeoTuk011 is new and innovative company providing exciting services in the field of tourism and
                sightseeing.
                We are looking for number of enthusiastic and motivated people to join our small team in the role of sightseeing driver/guide
                in the part time,
                casual and/or full time roles.
                This job could complement your studying or another part time job or could be your full time position</p>

            <p class="job-align">If you:</p>

            <ul class="job-align">
                <li>Have great personality and sense of humour</li>
                <li>Are very outgoing, sociable and love communicating with the people coming to see our city</li>
                <li>Have good spoken English (another language would be an advantage)</li>
                <li>Are passionate about both, our country, its culture, and more specifically about Belgrade and its
                    history, popular spots and landmarks
                </li>
                <li>Have B class drivers licence and feel comfortable driving through city centre during peak hours.
                </li>
            </ul>

            <p><strong>Then we need you.</strong></p>
            <p class="job-align">We offer:</p>
            <ul class="job-align">
                <li>Necessary training</li>
                <li>Flexible hours within the team</li>
                <li>Independent and creative work environment</li>
                <li>Excellent base salary and generous performance-based bonuses</li>
            </ul>
            <br>
            <p>Please send your letter of application and CV (your choice, in English or Serbian) by 20.02.2108 on
                <a href="mailto:info@beotuk.com" target="_top">info@beotuk.com</a></p>


            <p>Preferred candidates will be contacted before 22.02.2018.</p>

        </div>

@endsection