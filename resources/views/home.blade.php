@extends('index')

@section('content')

    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <header id="HOME">
        <div class="section_overlay">
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#HOME">Home</a></li>
                            <li><a href="#SERVICE">Services</a></li>
                            <li><a href="#ABOUT">About</a></li>
                            <li><a href="#TESTIMONIAL">Testimonial</a></li>
                            <li><a href="#WORK">Work</a></li>
                            <li><a href="#CONTACT">Contact</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </nav>

            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="home_text wow fadeInUp animated">
                            <h1>uroš mirić</h1>
                            <p>Software engineer</p>
                            <img src="images/shape.png" alt="shape">
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="scroll_down">
                            <a href="#SERVICE"><img src="images/scroll.png" alt="scroll"></a>
                            <p>Scroll Down</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </header>

    <section class="services" id="SERVICE">
        <div class="container">
            <div class="row">
                <div class="col-md-3 text-center">
                    <div class="single_service wow fadeInUp" data-wow-delay="1s">
                        <i class="fas fa-cogs"></i>
                        <h2>Backend</h2>
                        <p>Strong knowledge of PHP frameworks, and understanding of MVC design patterns. Experience in
                            building of APIs for all platforms</p>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="single_service wow fadeInUp" data-wow-delay="2s">
                        <i class="fas fa-cloud"></i>
                        <h2>Frontend</h2>
                        <p>In love with NodeJS (playing with Raspberry Pi), and knowledge of other JavaScript frameworks
                            (React, Angular, Vue).</p>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="single_service wow fadeInUp" data-wow-delay="3s">
                        <i class="fas fa-database"></i>
                        <h2>Databases</h2>
                        <p>Advanced SQL querying techniques.</p>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="single_service wow fadeInUp" data-wow-delay="4s">
                        <i class="fas fa-search"></i>
                        <h2>Seo</h2>
                        <p>Experience with optimising websites to achieve business goals.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about_us_area" id="ABOUT">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="about_title">
                        <h2>About Me</h2>
                        <img src="images/shape.png" alt="shape">
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6  wow fadeInRight animated">
                    <p class="about_us_p">Over seven years in IT as a developer.
                        Development of software on various projects per client’s requirements
                        working in various PHP frameworks (Laravel (mainly), FuelPHP, CodeIgniter) LAMP developer.
                    </p>
                    <p>Experience with design, development and maintenance of MySQL database.</p>
                </div>
                <div class="col-md-6  wow fadeInRight animated">
                    <p class="about_us_p">Ability to work well with all levels of the organization.
                        Experience with working both remotely, and in company.
                    </p>
                    <p>
                        Intern supervising and Talent Management experience.
                        Team Leadership Experience, Product Knowledge and Industry Experience,
                        Leadership Skills, Strong Oral and Written Communication Skills
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonial text-center wow fadeInUp animated" id="TESTIMONIAL">
        <div class="container">
            <div class="icon">
                <i class="icon-quote"></i>
            </div>
            <div class="owl-carousel">
                <div class="single_testimonial text-center wow fadeInUp animated">
                    <p>If you always do what you’ve always done, you’ll always get what you’ve always got.</p>
                    <h4>-HENRY FORD</h4>
                </div>
                <div class="single_testimonial text-center">
                    <p>It's not the mountain we counquer, but ourselves.</p>
                    <h4>-EDMUND HILLARY</h4>
                </div>
            </div>
        </div>
    </section>

    <div class="fun_facts">
        <section class="header parallax home-parallax page" id="fun_facts">
            <div class="section_overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 wow fadeInLeft animated">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="single_count">
                                        <i class="icon-toolbox"></i>
                                        <h3>67</h3>
                                        <p>Project Done</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="single_count">
                                        <i class="icon-clock"></i>
                                        <h3>5000+</h3>
                                        <p>Hours Worked</p>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="single_count">
                                        <i class="icon-trophy"></i>
                                        <h3>3</h3>
                                        <p>Awards Won</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="work_area" id="WORK">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="work_title  wow fadeInUp animated">
                        <div class="work_title">
                            <h2>Some Cool Projects</h2>
                        </div>
                        <img src="images/shape.png" alt="shape">
                        <p>Lot of NDA projects.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 no_padding">
                    <div class="single_image">
                        <img src="images/crowdfooding.png" alt="Crowdfooding">
                        <div class="image_overlay">
                            <a target="_blank" href="http://forwardfooding.com/">View Full Project</a>
                            <h2>Crowdfunding Platform</h2>
                            <h4>Laravel - JavaScript - Full Stack Development - From scratch</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 no_padding">
                    <div class="single_image">
                        <img src="images/bebinar.png" alt="Bebinar">
                        <div class="image_overlay">
                            <a target="_blank" href="http://www.bebinar.rs/">View Full Project</a>
                            <h2>Online Parent Tutorial</h2>
                            <h4>Laravel - From Scratch</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 no_padding">
                    <div class="single_image">
                        <img src="images/w4.jpg" alt="Smart Email System">
                        <div class="image_overlay">
                            <a target="_blank" href="http://sameplace.com/">View Full Project</a>
                            <h2>Smart Email System - Start Up</h2>
                            <h4>Laravel - AngularJS - From scratch</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pad_top">
                <div class="col-md-4 no_padding">
                    <div class="single_image">
                        <img src="images/charlotte-skyline.jpg" alt="Charlotte Website">
                        <div class="image_overlay">
                            <a target="_blank" href="http://www.rentuptowncharlotte.com/">View Full Project</a>
                            <h2>Renting Website</h2>
                            <h4>Wordpress - From scratch</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 no_padding">
                    <div class="single_image">
                        <img src="images/sbb-logo.jpg" alt="SBB">
                        <div class="image_overlay">
                            <a target="_blank" href="http://www.sbb.rs">View Full Project</a>
                            <h2>Official SBB Website</h2>
                            <h4>PHP - JavaScript - Being part of project</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 no_padding">
                    <div class="single_image">
                        <img src="images/charl.jpg" alt="Charlotte">
                        <div class="image_overlay">
                            <a target="_blank" href="http://www.livinguptowncharlotte.com/">View Full Project</a>
                            <h2>Renting Website Charlotte</h2>
                            <h4>PHP - Admin area, and some features</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 no_padding">
                    <div class="single_image">
                        <img src="images/spinclicker.png" alt="Charlotte">
                        <div class="image_overlay">
                            <a target="_blank" href="http://spinclicker.com/">View Full Project</a>
                            <h2>Spinclicker toy sale website</h2>
                            <h4>Laravel - Bootstrap - From Scratch</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 no_padding">
                    <div class="single_image">
                        <img src="images/spill.png" alt="spill">
                        <div class="image_overlay">
                            <a target="_blank" href="http://getspill.com/">View Full Project</a>
                            <h2>Social Network</h2>
                            <h4>Laravel - RESTful API from scratch for mobile app</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="call_to_action">
        <div class="container">
            <div class="row">
                <div class="col-md-8 wow fadeInLeft animated">
                    <div class="left">
                        <h2>LOOKING FOR EXPERIENCED SOFTWARE ENGINEER?</h2>
                        <p>I am always interested in new opportunities, and new challenges.</p>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-1 wow fadeInRight animated">
                    <div class="baton">
                        <a href="#CONTACT">
                            <button type="button" class="btn btn-primary cs-btn">Let's Talk</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contact" id="CONTACT">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="contact_title  wow fadeInUp animated">
                        <div class="work_title">
                            <h2>- get in touch -</h2>
                        </div>
                        <ul class="icon_list first">
                            <li><a href="https://www.facebook.com/papaurke" target="_blank"><i
                                            class="fab fa-facebook-square"></i></a></li>
                            <li><a href="https://www.instagram.com/papaurke/" target="_blank"><i
                                            class="fab fa-instagram"></i></a></li>
                            <li><a href="https://plus.google.com/u/0/111255794933343439573" target="_blank"><i
                                            class="fab fa-google-plus-square" target="_blank"></i></a></li>
                            <li><a href="https://rs.linkedin.com/in/uros-miric-618b1395" target="_blank"><i
                                            class="fab fa-linkedin"></i></a></li>
                            <li><a href="https://twitter.com/papaurke" target="_blank"><i
                                            class="fab fa-twitter-square"></i></a>
                            </li>
                        </ul>
                        <img src="images/shape.png" alt="shape">
                        <p>Always interested in new opportunities, and new challenges.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4  wow fadeInLeft animated">
                    <div class="single_contact_info">
                        <div class="contact wow fadeInUp" data-wow-delay="1s">
                            <i class="fas fa-phone"></i>
                        </div>
                        <h2>Call Me</h2>
                        <p>+381 69 505 8899</p>
                    </div>
                </div>
                <div class="col-md-4  wow fadeInLeft animated">

                    <div class="single_contact_info">
                        <div class="contact wow fadeInUp" data-wow-delay="1s">
                            <i class="fas fa-envelope-square"></i>
                        </div>
                        <h2>Email Me</h2>
                        <p><a href="mailto:urosmiric88@gmail.com?Subject=Hello Uros">urosmiric88@gmail.com</a></p>
                    </div>
                </div>
                <div class="col-md-4  wow fadeInLeft animated">

                    <div class="single_contact_info">
                        <div class="contact wow fadeInUp" data-wow-delay="1s">
                            <i class="fas fa-building"></i>
                        </div>
                        <h2>City</h2>
                        <p>Belgrade, Serbia</p>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="work-with   wow fadeInUp animated">
                        <h3>looking forward to hearing from you!</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
