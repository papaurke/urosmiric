<footer>
    <div class="container">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center   wow fadeInUp animated">
                    <div class="social">
                        <h2>Follow Me on Here</h2>
                        <ul class="icon_list">
                            <li><a href="https://www.facebook.com/papaurke" target="_blank"><i
                                            class="fab fa-facebook-square"></i></a></li>
                            <li><a href="https://www.instagram.com/papaurke/" target="_blank"><i
                                            class="fab fa-instagram"></i></a></li>
                            <li><a href="https://plus.google.com/u/0/111255794933343439573" target="_blank"><i
                                            class="fab fa-google-plus-square" target="_blank"></i></a></li>
                            <li><a href="https://rs.linkedin.com/in/uros-miric-618b1395" target="_blank"><i
                                            class="fab fa-linkedin"></i></a></li>
                            <li><a href="https://twitter.com/papaurke" target="_blank"><i class="fab fa-twitter-square"></i></a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
</footer>


<!-- =========================
     SCRIPTS
============================== -->

<script type="text/javascript" src="{{ URL::asset('/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/jquery.nicescroll.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/wow.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('/js/script.js') }}"></script>


</body>

</html>


<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-93169164-1', 'auto');
    ga('send', 'pageview');

</script>